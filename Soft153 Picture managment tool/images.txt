Name,Date,Category,Description,Comments
cat.jpg,02/05/2004,Pets,Cat,Cat on sofa
dog.jpg,04/03/2000,Pets,Dog,Cute dog
lake.jpg,05/04/1998,Holiday,a view of the lake,Taken on a day out with family
bridge.jpg,05/04/1997,Holiday,a view of the golden gate bridge,Taken with a drone
rugby.jpg,17/04/2000,Sports,World cup try,England won the game
soccar.jpg,13/05/2001,Sports,Messi scores a goal,Great game of football
sunflower.jpg,21/07/1993,Plants,biggest sunflower ever,taken on a summer night walk
cactus.jpg,24/12/2003,Plants,An collection of arrays,taken on a summers walk during the day
israel.jpg, 01/01/2012,Holiday,Picture of captial, Cloudy day.