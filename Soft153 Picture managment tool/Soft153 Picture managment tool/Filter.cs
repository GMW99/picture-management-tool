﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soft153_Picture_managment_tool
{
    class Filter
    {
        /*
         * File structure is NAME,DATE,CATEGORY,DESCRIPTION,COMMENTS
         */
        private const int IMAGENAMEINDEX = 0;
        private const int DATEINDEX = 1;
        private const int CATEGORYINDEX = 2;
        private const int DESCRIPTIONINDEX = 3;
        private const int COMMENTSINDEX = 4;
        public string CategoryToMatch { set; private get; }
        public Boolean CategoryFlag { set; private get; }
        public Boolean DescriptionFlag { set; private get; }
        public Boolean CommentsFlag { set; private get; }
        public Boolean DateFlag { set; private get; }
        public Boolean NoneFlag { set; private get; }
        public Boolean AllFlag { set; private get; }
        public string DescriptionFilter { set; private get; }
        public string CommentsFilter { set; private get; }        
        public int MonthBegin { set; private get; }
        public int YearBegin { set; private get; }
        public int MonthEnd { set; private get; }
        public int YearEnd { set; private get; }
        public List<string> ImageList { private set; get; }

        public Filter()
        {
            ImageList = new List<string>();
            CategoryToMatch = "";
            DescriptionFilter = "";
            CategoryFlag = false;
            DescriptionFlag = false;
            CommentsFlag = false;
            DateFlag = false;
            NoneFlag = false;
            AllFlag = false;
            MonthBegin = 0;
            YearBegin = 0;
            MonthEnd = 0;
            YearEnd = 0;
        }

        /// <summary>
        /// Initialseing values so everything is set to default values.
        /// </summary>
        public void InitialiseStatistics()
        {
            ImageList = new List<string>();
            CategoryToMatch = "";
            DescriptionFilter = "";
            CategoryFlag = false;
            DescriptionFlag = false;
            CommentsFlag = false;
            DateFlag = false;
            NoneFlag = false;
            AllFlag = false;
            MonthBegin = 0;
            YearBegin = 0;
            MonthEnd = 0;
            YearEnd = 0;
            
        }

        /// <summary>
        /// Makes an array of the lineOfText
        /// the sees which flag has been set to then run 
        /// the apporiate ImageFilte method or not run.
        /// </summary>
        /// <param name="lineOfText"></param>
        public void ProcessLineOfText(string lineOfText)
        {
            string[] imagesInText;


            char[] delimiter = { ',' };

            imagesInText = lineOfText.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);


            if (CategoryFlag == true)
            {
                ImageFilter(imagesInText[IMAGENAMEINDEX], imagesInText[CATEGORYINDEX], CategoryToMatch);
            }
            else if (DescriptionFlag == true)
            {
                ImageFilter(imagesInText[IMAGENAMEINDEX], imagesInText[DESCRIPTIONINDEX], DescriptionFilter);
            }
            else if (CommentsFlag == true)
            {
                ImageFilter(imagesInText[IMAGENAMEINDEX], imagesInText[COMMENTSINDEX], CommentsFilter);
            }
            else if (DateFlag == true)
            {
                ImageFilter(MonthBegin, MonthEnd, YearBegin, YearEnd, imagesInText[IMAGENAMEINDEX], imagesInText[DATEINDEX]);
            }
            else if (NoneFlag == true)
            {
                ImageList.Add(imagesInText[IMAGENAMEINDEX]);
            }
            else if (AllFlag == true)
            {
                // Do nothing all items are filtered out since nothing is added to the ImageList
            }
        }

        /// <summary>
        /// checks to see if the text file has the TextToFind in it 
        /// and adds the image to the imageList if it does.
        /// </summary>
        /// <param name="image"> Image name</param>
        /// <param name="text">The text in the text file </param>
        /// <param name="textToFind">The text to searched for in text</param>
        public void ImageFilter(string image, string text, string textToFind)
        {

            if (text.ToUpper().Contains(textToFind.ToUpper()))
            {
                ImageList.Add(image);
            }
        }

        /// <summary>
        /// Checks too the dates the user wants to see then filters appropriatly so the images within the dates
        /// are only added to the ImageList
        /// </summary>
        /// <param name="monthBegin"></param>
        /// <param name="monthEnd">T</param>
        /// <param name="yearBegin"></param>
        /// <param name="yearEnd"></param>
        /// <param name="image">Image name to add to the imageList</param>
        /// <param name="text">Text from the file</param>
        public void ImageFilter( int monthBegin, int monthEnd, int yearBegin, int yearEnd, string image, string text)
        {
            const int MONTHINDATEINDEX = 1;
            const int YEARINDATEINDEX = 2;
            int month = 0;
            int year = 0;
            string[] date;
            char[] delimiter = { '/' };
            date = text.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
            month = Convert.ToInt16(date[MONTHINDATEINDEX]);
            year = Convert.ToInt32(date[YEARINDATEINDEX]);
            // Going from monthEnd to MonthBegin since month begin is bigger same with the years. 
            if (monthEnd < monthBegin && yearEnd < yearBegin) 
            {
                //Checking to see if month an year are between the values the user entered int
                if ((month >= monthBegin || month <= monthEnd) && (year >=yearBegin || year <= yearEnd))
                {
                    ImageList.Add(image);
                }
            }
            // Going from monthBegin to month end and going from yearEnd to yearBegin 
            else if (monthEnd > monthBegin && yearEnd < yearBegin) 
            {
                //Checking to see if month an year are between the values the user entered int
                if (month >= monthBegin && month <= monthEnd && year >= yearBegin || year <= yearEnd) 
                {
                    ImageList.Add(image);
                }
            }
            // Going from monthEnd to month begin and yearBegin to yearEnd
            else if (monthEnd < monthBegin && yearEnd > yearBegin)
            {
                //Checking to see if month an year are between the values the user entered int
                if (month >= monthBegin || month <= monthEnd && year >= yearBegin && year <= yearEnd)
                {
                    ImageList.Add(image);
                }
            }
            //Going from monthBegin to monthEnd and the same with the year 
            else if ( month >= monthBegin && month  <= monthEnd && year >= yearBegin && year <= yearEnd )
            {
                ImageList.Add(image);
            }
        }
    }
}
