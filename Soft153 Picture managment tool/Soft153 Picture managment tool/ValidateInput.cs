﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Soft153_Picture_managment_tool
{
    class ValidateInput
    {
        private const int DATEINDEX = 1;
        public bool IsValid { private set; get; }
        public string ValidationMessage { private set; get; }
        public string FileLocation { set; private get; }
        public string[] Date {private set;  get; } 
        public string Category { set; private get; }
        public ValidateInput()
        {
            ValidationMessage = "";
            Date = new string[4];
        }
        public void InitialiseInput()
        {
            IsValid = true;
            ValidationMessage = "";
            Date = new string[4];
        }

        /// <summary>
        /// This function is to check if there are five sections to the 
        /// line in the file and that the date is formatted correctly
        /// (having a dd/mm/yyyy structure)
        /// </summary>
        /// <param name="lineOfText"></param>
        public void ValidateLineOfText(string lineOfText)
        {
            string[] text;
            string[] date;
            char[] delimiter = { ',' };
            char[] dateDeliminter = { '/' };
            IsValid = true;

            
            text = lineOfText.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
            date = text[DATEINDEX].Split(dateDeliminter, StringSplitOptions.RemoveEmptyEntries);

            if (text.Count() != 5  || date.Count() != 3)
            {
                IsValid = false;
                ValidationMessage = "File has incorect formatting within " +
                    "it not all images will be shown.";
            }
        }


        /// <summary>
        /// This checks if the Category by checking if it is null since 
        /// if you have not selected a value in a combobox 
        /// it is default value is set to null
        /// </summary>
        public void ValidateCategory()
        {
            IsValid = true;
            if (Category == null)
            {
                IsValid = false;
                ValidationMessage = "Please select a category";
            }
        }
        /// <summary>
        ///  This checks is the File Exists in the location given
        ///  if it doenst then IsValid is set to false
        ///  and a validationMessage is set.
        /// </summary>
        public void ProcessFile()
        {
            IsValid = true;

            if (!File.Exists(FileLocation))
            {
                IsValid = false;
                ValidationMessage = "Image name is not correct or not a supported " +
                    "image type or image is not in the image folder that " +
                    "should be located where the text file is.";
            }

        }
        /// <summary>
        /// This is to check if the months entered in by the user in
        /// the PictureMangementForm are integer and the months are 
        /// within 1 and 12 if the input is an integer
        /// </summary>
        public void ProcessDate()
        {
            int endMonth = 0;
            int beginYear = 0;
            int endYear = 0;
            int beginMonth = 0;

            IsValid = true;

            try
            {
                beginMonth = Convert.ToInt32(Date[0]);
                endMonth = Convert.ToInt32(Date[1]);
                beginYear = Convert.ToInt32(Date[2]);
                endYear = Convert.ToInt32(Date[3]);
                if (!ValidMonthNumber(beginMonth) || !ValidMonthNumber(endMonth))
                {
                    IsValid = false;
                    ValidationMessage = "Please enter a month between 1 and 12";
                }
            }
            catch(FormatException ex)
            {
                IsValid = false;
                ValidationMessage = "Please enter a correct date";
            }
        }
        /// <summary>
        /// Checks if the monthToCheck is between the values 1 and 12 if
        /// it isnt it returns false otherwise it return true
        /// </summary>
        /// <param name="monthToCheck">Month to be checked to see if valid</param>
        /// <returns></returns>
        private bool ValidMonthNumber(int monthToCheck)
        {
            bool isValid = true;

            if (1 > monthToCheck ||  monthToCheck > 12)
            {
                isValid = false;
            }

            return isValid;

        }
    }
}
