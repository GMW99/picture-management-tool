﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Soft153_Picture_managment_tool
{
    public partial class PictureManagmentForm : Form
    {

        private string currentDirectoroyName;
        private string fileName;
        private string directoryName;
        private List<Boolean> isLineCorrectList= new List<Boolean>();
        private int imageIndex;

        /// <summary>
        /// This is used to Refesh The image shown if the ImageIndex 
        /// value is changed since if the index corrosponds to which
        /// image will be seen
        /// </summary>
        private int ImageIndex
        {
            set
            {
                imageIndex = value;
                RefreshDisplay();
               
            }
            get
            {
                return imageIndex;
            }
        }
        private Filter filter;

        private ValidateInput validateInput;
        public PictureManagmentForm()
        {
            InitializeComponent();
            filter = new Filter();
            validateInput = new ValidateInput();
        }

        /// <summary>
        /// If the openFileDialog is open and a file is selected the is stores the filename and the directory of where the images where stored.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() != DialogResult.Cancel)
            {
                currentDirectoroyName = openFileDialog.InitialDirectory;
                fileName = openFileDialog.FileName;
                directoryName = Path.GetDirectoryName(fileName);
            } // end if
        }
        /// <summary>
        /// This Loads the file into a string so it can be used by other methods
        /// everyline is read till the end of the file. If any line in the document is unreadable then a message box is shown.
        /// </summary>
        private void LoadFile()
        {
            string lineFromFile;

            try
            {
                using (StreamReader reader = new StreamReader(fileName))
                {
                    lineFromFile = reader.ReadLine(); // To skip the first line of the document because it is just the header of the file.
                    while (!reader.EndOfStream)
                    {
                        lineFromFile = reader.ReadLine();
                        ProcesslineFromFile(lineFromFile);
                    }
                }  // end using
                // This is see if there was any line that was not properly formatted in the document that is uploaded
                foreach (Boolean line in isLineCorrectList)
                {
                    if (line == false)
                    {
                        MessageBox.Show("There are some lines in the document that are incorrect so not all images will be displayed");
                        break;
                    }
                }
            }
            catch (ArgumentNullException ex)
            {
                MessageBox.Show("File Not Found");
            }
        }  // end LoadFile()

        /// <summary>
        /// This function checks if the line is valid useing the ValidateInputClass
        /// if is it then calls the ProcessLineOfText method from filter with the line of text
        /// then adds to the list the true other wise sets that elemement in the list to false.
        /// </summary>
        /// <param name="inputText"> This is the the line of text to be processed if valid</param>
        private void ProcesslineFromFile(string inputText)
        {
            validateInput.ValidateLineOfText(inputText);
            if (validateInput.IsValid)
            {
                filter.ProcessLineOfText(inputText);
                isLineCorrectList.Add(validateInput.IsValid);
            }
            else
            {
                isLineCorrectList.Add(validateInput.IsValid);
            }
        } // end of ProcesslineFromFile
        /// <summary>
        /// Resets all the properties of the classes.
        /// </summary>
        private void Initialise()
        {
            InitialiseLabels();
            filter.InitialiseStatistics();
            validateInput.InitialiseInput();

        }
        /// <summary>
        /// Sets up the labels to be 0.
        /// </summary>
        private void InitialiseLabels()
        {
            imageNumberLabel.Text = "0";
            amountOfImagesLabel.Text = "0";
        }

        /// <summary>
        /// Refeshes the display of the image based on the ImageIndex of the list of images
        /// it then checks if the file exsits and calls DrawImage and displays  an error message if the file doesnt exsit..
        /// </summary>
        private  void RefreshDisplay()
        {
            string imagesLocation;
            Image image;
            if (ImageIndex >= 0 && ImageIndex < filter.ImageList.Count)
            {
                imagesLocation = directoryName + @"\images\" + filter.ImageList[ImageIndex];
                validateInput.FileLocation = imagesLocation;
                /* This is the check if the file is real and that the image within it is acctualy there
                 * setting IsValid to true if it there other wise setting the image to null.
                 */
                validateInput.ProcessFile();
                if (validateInput.IsValid)
                {
                    image = new Bitmap(imagesLocation);
                    DrawImage(image);
                }
                else
                {
                    ClearImage();
                    MessageBox.Show(validateInput.ValidationMessage);
                }

                imageNumberLabel.Text = (ImageIndex + 1).ToString();
                amountOfImagesLabel.Text = filter.ImageList.Count.ToString();
                // Calls DrawImage with the image as ethier the image or null
                
            }
            else
            {
                InitialiseLabels();
                ClearImage();
            }
        }
        /// <summary>
        /// To clear the panel and make it white
        /// </summary>
        private void ClearImage()
        {
            using (Graphics panelGraphics = ImagePanel.CreateGraphics())
            {
                panelGraphics.Clear(Color.White);
            }
        }
        
        /// <summary>
        /// This draws the image in a rectangle if the location of the image provided is not null
        /// </summary>
        /// <param name="image">This is the image that will be displayed</param>
        private void DrawImage(Image image)
        {
            Rectangle picture = new Rectangle(20, 30, 250, 200);
            using ( Graphics panelGraphics = ImagePanel.CreateGraphics())
            {
                panelGraphics.Clear(Color.White);
                panelGraphics.DrawImage(image, picture);
            }
        }
        private void ImagePanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void noneRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// Checks which radio button has been selected and sees if the input associated with
        /// that button is valid  then sets a flag, to true based on the 
        /// button checked and input is valid, that the filter class will 
        /// deal with to make sure the correct filtering takes place.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void filterButton_Click(object sender, EventArgs e)
        {
            string category;
            string description;
            string comments;
            Initialise();
 
            if (noneRadioButton.Checked)
            {
                filter.NoneFlag = true;
            }
            
            else if (categoryRadioButton.Checked)
            {
                category = (string)categoryComboBox.SelectedItem;
                validateInput.Category = category;
                validateInput.ValidateCategory();
                if(validateInput.IsValid)
                {
                    filter.CategoryToMatch = category;
                    filter.CategoryFlag = true;
                }
                else
                {
                    MessageBox.Show(validateInput.ValidationMessage, "Category", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    filter.AllFlag = true;
                }

            }
            else if(descriptionRadioButton.Checked) 
            {
                description = descriptionTextBox.Text;
                filter.DescriptionFilter = description;
                filter.DescriptionFlag = true;
            }
            else if (commentsRadioButton.Checked)
            {
                comments = commentsTextBox.Text;
                filter.CommentsFilter = comments;
                filter.CommentsFlag = true;
            }
            else if (dateRadioButton.Checked)
            {
                //monthBegin = Convert.ToInt16(beginMonthTextBox.Text);

                // Setting up the Date Array validateInput
                validateInput.Date[0] = beginMonthTextBox.Text;
                validateInput.Date[1] = endMonthTextBox.Text;
                validateInput.Date[2] = beginYearTextBox.Text;
                validateInput.Date[3] = endYearTextBox.Text;
                // End of setting up Date Array in validateInput

                // Processing Date to check if all dates are valid.
                validateInput.ProcessDate();

                if (validateInput.IsValid) // If dates are valid set up the filter with the correct properities and set the DateFlag to be true.
                {
                    filter.MonthBegin = Convert.ToInt16(beginMonthTextBox.Text);
                    filter.MonthEnd = Convert.ToInt16(endMonthTextBox.Text);
                    filter.YearBegin = Convert.ToInt16(beginYearTextBox.Text);
                    filter.YearEnd = Convert.ToInt16(endYearTextBox.Text);
                    filter.DateFlag = true; 
                }
                else //Show messsage box 
                {
                    MessageBox.Show(validateInput.ValidationMessage, "Wrong Date", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    filter.AllFlag = true;
                }

            }
            LoadFile();
            ImageIndex = 0;
            //Set the ImageIndex to zero which will in turn refesh the displays.
        }

        /// <summary>
        /// This increase the value of the Image index by one 
        /// as long as it is within the range of the list of images
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nextButton_Click(object sender, EventArgs e)
        {
            if (ImageIndex < filter.ImageList.Count - 1)
            {
                ImageIndex += 1;
            }
        }
        /// <summary>
        /// This increase the value of the Image index by one 
        /// as long as it is within the range of the list of images
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void previousButton_Click(object sender, EventArgs e)
        {
            if (0 < ImageIndex)
            {
                ImageIndex -= 1;
            }
        }

        private void categoryComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
    }
}

